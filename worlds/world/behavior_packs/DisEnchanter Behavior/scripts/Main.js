import {system, world, ItemStack, ItemTypes} from "@minecraft/server";
import { Enchantments } from "classes/Enchantments.js";

//------------------------------------------------
//Adjust Values Here
let amethystNeedAmount = 1;
let DisenchantTimeTicks = 20; // 20 ticks = 1 second/s
//------------------------------------------------

system.events.beforeWatchdogTerminate.subscribe((eventData)=>{
	eventData.cancel = true;
});

system.runInterval(function Disenchanting(){
	let queryTable = {type:"disenchanter:disenchanting_table",excludeTags:["disenchanting"]};
	let getDisenchantingTables = Array.from(world.getDimension(`overworld`).getEntities(queryTable));
	for(let table of getDisenchantingTables){
				
		let container = table.getComponent('inventory').container;
		
		//////////////////////////////////////
		//Display Floating Items
		//////////////////////////////////////			
		let queryTableSlots = {type:"disenchanter:slot",location:table.location,maxDistance:1,closest:9};
		let getTableSlots = table.dimension.getEntities(queryTableSlots);	
		for(let slotIndex=0; slotIndex<=8; slotIndex++){
			for(let slot of getTableSlots){
				if(!slot.nameTag.includes(slotIndex)) continue;
				let tableSlotItem = container.getItem(slotIndex);
				if(!tableSlotItem) {
					slot.runCommandAsync(`replaceitem entity @s slot.weapon.mainhand 0 air`);
					continue;
				}
				switch(tableSlotItem.typeId){
					case "minecraft:shield":
						slot.runCommandAsync(`replaceitem entity @s slot.weapon.mainhand 0 disenchanter:shield_item`);
						break;
					case "minecraft:trident":
						slot.runCommandAsync(`replaceitem entity @s slot.weapon.mainhand 0 disenchanter:trident_item`);
						break;
					case "minecraft:crossbow":
						slot.runCommandAsync(`replaceitem entity @s slot.weapon.mainhand 0 disenchanter:crossbow_item`);
						break;
					case "minecraft:bow":
						slot.runCommandAsync(`replaceitem entity @s slot.weapon.mainhand 0 disenchanter:bow_item`);
						break;
					default:
						slot.runCommandAsync(`replaceitem entity @s slot.weapon.mainhand 0 ${tableSlotItem.typeId}`);
				}				
				if(Enchantments.getEnchants(tableSlotItem).length){							
					slot.runCommandAsync(`enchant @s sharpness 1`)
					slot.runCommandAsync(`enchant @s protection 1`)
					slot.runCommandAsync(`enchant @s loyalty 1`)
					slot.runCommandAsync(`enchant @s unbreaking 1`)		
				}
				//if(!slot.getComponent('inventory').container.getItem(slotSize)) continue;
				//slot.runCommandAsync(`say ${slot.getComponent('inventory').container.getItem(slotSize).typeId}`);				
			}						
		}

		//////////////////////////////////////
		//Remove Excess Items
		//Remove Non-Book Items
		//Remove Non-Amethyst Item
		//////////////////////////////////////

		for(let slotIndex=0; slotIndex<=9; slotIndex++){
			let item = container.getItem(slotIndex);
			if(!item) continue;
			if(slotIndex >= 0 && slotIndex <=8 && item.amount>1){
				item.amount = item.amount-1
				table.dimension.spawnItem(item,table.location);
				item.amount = 1
				container.setItem(slotIndex,item);
			}
			if(slotIndex >= 1 && slotIndex <=8 && item.typeId != "minecraft:book" && item.typeId != "minecraft:enchanted_book"){
				table.dimension.spawnItem(item,table.location);
				container.setItem(slotIndex,undefined);
			}
			if(slotIndex == 9 && item.typeId != "minecraft:amethyst_shard"){
				table.dimension.spawnItem(item,table.location);
				container.setItem(slotIndex,undefined);
			}
		}

		//////////////////////////////////////
		//Disenchanting Items
		//////////////////////////////////////

		if(!hasNecessaryItems(container)) continue;
		table.addTag("disenchanting");
		
		let scheduleId = system.runInterval(function Disenchanting(){
			if(!hasNecessaryItems(container)) {
				table.removeTag("disenchanting");
				system.clearRun(scheduleId);				
				return;
			}
			let location = {x:table.location.x,y:table.location.y,z:table.location.z};
			let getEnchantedItem = container.getItem(0);
			let getEnchants = Enchantments.getEnchants(getEnchantedItem);
			
			for(let slotIndex=1; 8>=slotIndex; slotIndex++){				
				let book = container.getItem(slotIndex);
				if(!book) continue;
				if(book.typeId != "minecraft:book") continue;
				if(Enchantments.getEnchants(book).length != 0) continue;
				if(getEnchants.length <= 0) break;
				let newBook = Enchantments.addEnchant(new ItemStack(ItemTypes.get('minecraft:enchanted_book')),getEnchants[0]);
				let newEnchantedItem = Enchantments.removeEnchant(getEnchantedItem,getEnchants[0]);
				container.setItem(slotIndex,newBook);
				
				if(Enchantments.getEnchants(newEnchantedItem).length == 0 && getEnchantedItem.typeId == 'minecraft:enchanted_book'){
					container.setItem(0,new ItemStack(ItemTypes.get('minecraft:book'),1));
				}else{
					container.setItem(0,newEnchantedItem);
				}
				break;
			}
			
			let amethystItem = container.getItem(9);
			let amethystCurrentAmount = amethystItem.amount;
			
			if(amethystCurrentAmount-amethystNeedAmount>0){
				container.setItem(9, new ItemStack(ItemTypes.get(amethystItem.typeId),amethystCurrentAmount-amethystNeedAmount));
			}else{
				container.setItem(9, undefined);	
			}
			
			playDisenchantSound(location);
			table.removeTag("disenchanting");
			system.clearRun(scheduleId);
		},DisenchantTimeTicks);
	}	
},10);

function hasNecessaryItems(container){
	let enchantedItems = 0;
	let books = 0;
	let amethyst = 0;
	
	for(let slotIndex=1; 8>=slotIndex; slotIndex++){	
		let item = container.getItem(slotIndex);
		if(!item) continue;	
		if(item.typeId == "minecraft:book") books++;						
	}
	let getAmethystShardItem = container.getItem(9);
	let getEnchantedItem = container.getItem(0);
	if(!getEnchantedItem) return false;
	if(!getAmethystShardItem) return false;
	if(Enchantments.getEnchants(getEnchantedItem).length > 0) enchantedItems++;	
	if(getAmethystShardItem.typeId == "minecraft:amethyst_shard") amethyst = getAmethystShardItem.amount;	
	if(amethyst < amethystNeedAmount || books == 0 || enchantedItems == 0) return false;
	return true;
}

function playDisenchantSound(location){
	let queryPlayer = {families:["player"],location:location,maxDistance:3};
	let getNearestPlayers = world.getDimension('overworld').getEntities(queryPlayer);
	for(let player of getNearestPlayers){
		player.playSound("block.enchanting_table.use");
	}
}