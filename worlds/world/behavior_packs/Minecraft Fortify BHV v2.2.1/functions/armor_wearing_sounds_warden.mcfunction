

execute @a[hasitem={item=yes:warden_helmet,location=slot.armor.head},tag=!wearing_warden_helmet] ~~~ playsound armor.equip_generic @a[r=5] ~~1~ 0.7 0.8
execute @a[hasitem={item=yes:warden_helmet,location=slot.armor.head},tag=!wearing_warden_helmet] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.75
execute @a[hasitem={item=yes:warden_helmet,location=slot.armor.head},tag=!wearing_warden_helmet] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.6
execute @a[hasitem={item=yes:warden_helmet,location=slot.armor.head},tag=!wearing_warden_helmet] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.5
execute @a[hasitem={item=yes:warden_helmet,location=slot.armor.head},tag=!wearing_warden_helmet] ~~~ tag @s add wearing_warden_helmet
execute @a[hasitem={item=yes:warden_helmet,location=slot.armor.head,quantity=0},tag=wearing_warden_helmet] ~~~tag @s remove wearing_warden_helmet

execute @a[hasitem={item=yes:warden_chestplate,location=slot.armor.chest},tag=!wearing_warden_chestplate] ~~~ playsound armor.equip_generic @a[r=5] ~~1~ 0.7 0.8
execute @a[hasitem={item=yes:warden_chestplate,location=slot.armor.chest},tag=!wearing_warden_chestplate] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.75
execute @a[hasitem={item=yes:warden_chestplate,location=slot.armor.chest},tag=!wearing_warden_chestplate] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.6
execute @a[hasitem={item=yes:warden_chestplate,location=slot.armor.chest},tag=!wearing_warden_chestplate] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.5
execute @a[hasitem={item=yes:warden_chestplate,location=slot.armor.chest},tag=!wearing_warden_chestplate] ~~~ tag @s add wearing_warden_chestplate
execute @a[hasitem={item=yes:warden_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_warden_chestplate] ~~~tag @s remove wearing_warden_chestplate

execute @a[hasitem={item=yes:warden_leggings,location=slot.armor.legs},tag=!wearing_warden_leggings] ~~~ playsound armor.equip_generic @a[r=5] ~~1~ 0.7 0.8
execute @a[hasitem={item=yes:warden_leggings,location=slot.armor.legs},tag=!wearing_warden_leggings] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.75
execute @a[hasitem={item=yes:warden_leggings,location=slot.armor.legs},tag=!wearing_warden_leggings] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.6
execute @a[hasitem={item=yes:warden_leggings,location=slot.armor.legs},tag=!wearing_warden_leggings] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.5
execute @a[hasitem={item=yes:warden_leggings,location=slot.armor.legs},tag=!wearing_warden_leggings] ~~~ tag @s add wearing_warden_leggings
execute @a[hasitem={item=yes:warden_leggings,location=slot.armor.legs,quantity=0},tag=wearing_warden_leggings] ~~~tag @s remove wearing_warden_leggings

execute @a[hasitem={item=yes:warden_boots,location=slot.armor.feet},tag=!wearing_warden_boots] ~~~ playsound armor.equip_generic @a[r=5] ~~1~ 0.7 0.8
execute @a[hasitem={item=yes:warden_boots,location=slot.armor.feet},tag=!wearing_warden_boots] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.75
execute @a[hasitem={item=yes:warden_boots,location=slot.armor.feet},tag=!wearing_warden_boots] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.6
execute @a[hasitem={item=yes:warden_boots,location=slot.armor.feet},tag=!wearing_warden_boots] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 1 0.5
execute @a[hasitem={item=yes:warden_boots,location=slot.armor.feet},tag=!wearing_warden_boots] ~~~ tag @s add wearing_warden_boots
execute @a[hasitem={item=yes:warden_boots,location=slot.armor.feet,quantity=0},tag=wearing_warden_boots] ~~~tag @s remove wearing_warden_boots

