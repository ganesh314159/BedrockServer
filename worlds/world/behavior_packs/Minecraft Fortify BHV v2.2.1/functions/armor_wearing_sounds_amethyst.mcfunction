execute @a[hasitem={item=yes:amethyst_helmet,location=slot.armor.head},tag=!wearing_amethyst_helmet] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:amethyst_helmet,location=slot.armor.head},tag=!wearing_amethyst_helmet] ~~~ tag @s add wearing_amethyst_helmet
execute @a[hasitem={item=yes:amethyst_helmet,location=slot.armor.head,quantity=0},tag=wearing_amethyst_helmet] ~~~tag @s remove wearing_amethyst_helmet

execute @a[hasitem={item=yes:amethyst_chestplate,location=slot.armor.chest},tag=!wearing_amethyst_chestplate] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:amethyst_chestplate,location=slot.armor.chest},tag=!wearing_amethyst_chestplate] ~~~ tag @s add wearing_amethyst_chestplate
execute @a[hasitem={item=yes:amethyst_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_amethyst_chestplate] ~~~tag @s remove wearing_amethyst_chestplate

execute @a[hasitem={item=yes:amethyst_leggings,location=slot.armor.legs},tag=!wearing_amethyst_leggings] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:amethyst_leggings,location=slot.armor.legs},tag=!wearing_amethyst_leggings] ~~~ tag @s add wearing_amethyst_leggings
execute @a[hasitem={item=yes:amethyst_leggings,location=slot.armor.legs,quantity=0},tag=wearing_amethyst_leggings] ~~~tag @s remove wearing_amethyst_leggings

execute @a[hasitem={item=yes:amethyst_boots,location=slot.armor.feet},tag=!wearing_amethyst_boots] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:amethyst_boots,location=slot.armor.feet},tag=!wearing_amethyst_boots] ~~~ tag @s add wearing_amethyst_boots
execute @a[hasitem={item=yes:amethyst_boots,location=slot.armor.feet,quantity=0},tag=wearing_amethyst_boots] ~~~tag @s remove wearing_amethyst_boots

