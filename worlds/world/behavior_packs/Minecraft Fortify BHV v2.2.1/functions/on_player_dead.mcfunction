

#	 ===== Dead detect =====
tag @a add dead
tag @e[type=player] remove dead



scoreboard players set @a[tag=dead] weakness 0
scoreboard objectives add weapon_sound dummy



#	 ===== Copper =====
scoreboard players set @a[tag=dead] hammer 0
scoreboard players set @a[tag=dead] hammeranimation 0
tag @a[tag=dead] remove copper_hammer_charging
tag @a[tag=dead] remove copper_hammer_attacking
tag @a[tag=dead] remove copper_hammer_finished
tag @a[tag=dead] remove copper_hammer_chargingb
tag @a[tag=dead] remove copper_hammer_attackingb
tag @a[tag=dead] remove copper_hammer_finishedb
tag @a[tag=dead] remove using_copper_hammer


#	 ===== Amethyst =====
scoreboard players set @a[tag=dead] spear_attacking 0
tag @a[tag=dead] remove spear_attacking


#	 ===== Gilded Netherite =====
scoreboard players set @a[tag=dead] gilded_health -1


#	 ===== Glowing Obsidian =====
scoreboard players set @a[tag=dead] battleaxe 0
tag @a[tag=dead] remove using_battleaxe
tag @a[tag=dead] remove battleaxe_attacking
tag @a[tag=dead] remove battleaxe_hitting


#	 ===== Warden =====
scoreboard players set @a[tag=dead] holding_staff -1
tag @a[tag=dead] remove using_staff


#	 ===== Phantom =====
scoreboard players set @a[tag=dead] whip_animation 0
scoreboard players set @a[tag=dead] whip_attack 0
scoreboard players set @a[tag=dead] whip_attacking 0
scoreboard objectives add whip_detect dummy
execute @a[scores={whip_animation=1..7}] ~~~ scoreboard players set @e[type=yes:player_entity] whip_detect 1
execute @e[type=yes:player_entity,scores={whip_detect=0}] ~~~ scoreboard objectives setdisplay list 
execute @e[type=yes:player_entity,scores={whip_detect=0}] ~~~ scoreboard players reset @e[type=yes:player_entity] whip_detect
execute @a[scores={whip_animation=1..7}] ~~~ scoreboard players set @e[type=yes:player_entity] whip_detect 0
tag @a[tag=dead] remove using_whip
tag @a[tag=dead] remove whip_attacking

#	 ===== Aetherite =====
scoreboard players set @a[tag=dead] greatsword_atk -1
scoreboard players set @a[tag=dead] greatsword -1
scoreboard players set @a[tag=dead] greatsword_anim 0
tag @a[tag=dead] remove greatsword_loop