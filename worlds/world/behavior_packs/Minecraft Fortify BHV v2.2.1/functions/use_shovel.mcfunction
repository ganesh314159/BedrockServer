summon yes:on_use_entity ~~~
fill ~~~ ~~~ grass_path -1 replace grass
fill ~~~ ~~~ grass_path -1 replace dirt
fill ~~~ ~~~ grass_path 1 replace podzol
fill ~~~ ~~~ grass_path -1 replace mycelium
fill ~~~ ~~~ grass_path -1 replace dirt_with_roots
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ grass_path -1 playsound use.grass @p ~~1~ 1 0.9