execute @a[hasitem={item=yes:copper_helmet,location=slot.armor.head},tag=!wearing_copper_helmet] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.2
execute @a[hasitem={item=yes:copper_helmet,location=slot.armor.head},tag=!wearing_copper_helmet] ~~~ tag @s add wearing_copper_helmet
execute @a[hasitem={item=yes:copper_helmet,location=slot.armor.head,quantity=0},tag=wearing_copper_helmet] ~~~tag @s remove wearing_copper_helmet

execute @a[hasitem={item=yes:copper_chestplate,location=slot.armor.chest},tag=!wearing_copper_chestplate] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.2
execute @a[hasitem={item=yes:copper_chestplate,location=slot.armor.chest},tag=!wearing_copper_chestplate] ~~~ tag @s add wearing_copper_chestplate
execute @a[hasitem={item=yes:copper_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_copper_chestplate] ~~~tag @s remove wearing_copper_chestplate

execute @a[hasitem={item=yes:copper_leggings,location=slot.armor.legs},tag=!wearing_copper_leggings] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.2
execute @a[hasitem={item=yes:copper_leggings,location=slot.armor.legs},tag=!wearing_copper_leggings] ~~~ tag @s add wearing_copper_leggings
execute @a[hasitem={item=yes:copper_leggings,location=slot.armor.legs,quantity=0},tag=wearing_copper_leggings] ~~~tag @s remove wearing_copper_leggings

execute @a[hasitem={item=yes:copper_boots,location=slot.armor.feet},tag=!wearing_copper_boots] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.2
execute @a[hasitem={item=yes:copper_boots,location=slot.armor.feet},tag=!wearing_copper_boots] ~~~ tag @s add wearing_copper_boots
execute @a[hasitem={item=yes:copper_boots,location=slot.armor.feet,quantity=0},tag=wearing_copper_boots] ~~~tag @s remove wearing_copper_boots


execute @a[hasitem={item=yes:exposed_copper_helmet,location=slot.armor.head},tag=!wearing_exposed_copper_helmet] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:exposed_copper_helmet,location=slot.armor.head},tag=!wearing_exposed_copper_helmet] ~~~ tag @s add wearing_exposed_copper_helmet
execute @a[hasitem={item=yes:exposed_copper_helmet,location=slot.armor.head,quantity=0},tag=wearing_exposed_copper_helmet] ~~~tag @s remove wearing_exposed_copper_helmet

execute @a[hasitem={item=yes:exposed_copper_chestplate,location=slot.armor.chest},tag=!wearing_exposed_copper_chestplate] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:exposed_copper_chestplate,location=slot.armor.chest},tag=!wearing_exposed_copper_chestplate] ~~~ tag @s add wearing_exposed_copper_chestplate
execute @a[hasitem={item=yes:exposed_copper_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_exposed_copper_chestplate] ~~~tag @s remove wearing_exposed_copper_chestplate

execute @a[hasitem={item=yes:exposed_copper_leggings,location=slot.armor.legs},tag=!wearing_exposed_copper_leggings] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:exposed_copper_leggings,location=slot.armor.legs},tag=!wearing_exposed_copper_leggings] ~~~ tag @s add wearing_exposed_copper_leggings
execute @a[hasitem={item=yes:exposed_copper_leggings,location=slot.armor.legs,quantity=0},tag=wearing_exposed_copper_leggings] ~~~tag @s remove wearing_exposed_copper_leggings

execute @a[hasitem={item=yes:exposed_copper_boots,location=slot.armor.feet},tag=!wearing_exposed_copper_boots] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1.1
execute @a[hasitem={item=yes:exposed_copper_boots,location=slot.armor.feet},tag=!wearing_exposed_copper_boots] ~~~ tag @s add wearing_exposed_copper_boots
execute @a[hasitem={item=yes:exposed_copper_boots,location=slot.armor.feet,quantity=0},tag=wearing_exposed_copper_boots] ~~~tag @s remove wearing_exposed_copper_boots


execute @a[hasitem={item=yes:weathered_copper_helmet,location=slot.armor.head},tag=!wearing_weathered_copper_helmet] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1
execute @a[hasitem={item=yes:weathered_copper_helmet,location=slot.armor.head},tag=!wearing_weathered_copper_helmet] ~~~ tag @s add wearing_weathered_copper_helmet
execute @a[hasitem={item=yes:weathered_copper_helmet,location=slot.armor.head,quantity=0},tag=wearing_weathered_copper_helmet] ~~~tag @s remove wearing_weathered_copper_helmet

execute @a[hasitem={item=yes:weathered_copper_chestplate,location=slot.armor.chest},tag=!wearing_weathered_copper_chestplate] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1
execute @a[hasitem={item=yes:weathered_copper_chestplate,location=slot.armor.chest},tag=!wearing_weathered_copper_chestplate] ~~~ tag @s add wearing_weathered_copper_chestplate
execute @a[hasitem={item=yes:weathered_copper_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_weathered_copper_chestplate] ~~~tag @s remove wearing_weathered_copper_chestplate

execute @a[hasitem={item=yes:weathered_copper_leggings,location=slot.armor.legs},tag=!wearing_weathered_copper_leggings] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1
execute @a[hasitem={item=yes:weathered_copper_leggings,location=slot.armor.legs},tag=!wearing_weathered_copper_leggings] ~~~ tag @s add wearing_weathered_copper_leggings
execute @a[hasitem={item=yes:weathered_copper_leggings,location=slot.armor.legs,quantity=0},tag=wearing_weathered_copper_leggings] ~~~tag @s remove wearing_weathered_copper_leggings

execute @a[hasitem={item=yes:weathered_copper_boots,location=slot.armor.feet},tag=!wearing_weathered_copper_boots] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 1
execute @a[hasitem={item=yes:weathered_copper_boots,location=slot.armor.feet},tag=!wearing_weathered_copper_boots] ~~~ tag @s add wearing_weathered_copper_boots
execute @a[hasitem={item=yes:weathered_copper_boots,location=slot.armor.feet,quantity=0},tag=wearing_weathered_copper_boots] ~~~tag @s remove wearing_weathered_copper_boots


execute @a[hasitem={item=yes:oxidized_copper_helmet,location=slot.armor.head},tag=!wearing_oxidized_copper_helmet] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 0.9
execute @a[hasitem={item=yes:oxidized_copper_helmet,location=slot.armor.head},tag=!wearing_oxidized_copper_helmet] ~~~ tag @s add wearing_oxidized_copper_helmet
execute @a[hasitem={item=yes:oxidized_copper_helmet,location=slot.armor.head,quantity=0},tag=wearing_oxidized_copper_helmet] ~~~tag @s remove wearing_oxidized_copper_helmet

execute @a[hasitem={item=yes:oxidized_copper_chestplate,location=slot.armor.chest},tag=!wearing_oxidized_copper_chestplate] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 0.9
execute @a[hasitem={item=yes:oxidized_copper_chestplate,location=slot.armor.chest},tag=!wearing_oxidized_copper_chestplate] ~~~ tag @s add wearing_oxidized_copper_chestplate
execute @a[hasitem={item=yes:oxidized_copper_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_oxidized_copper_chestplate] ~~~tag @s remove wearing_oxidized_copper_chestplate

execute @a[hasitem={item=yes:oxidized_copper_leggings,location=slot.armor.legs},tag=!wearing_oxidized_copper_leggings] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 0.9
execute @a[hasitem={item=yes:oxidized_copper_leggings,location=slot.armor.legs},tag=!wearing_oxidized_copper_leggings] ~~~ tag @s add wearing_oxidized_copper_leggings
execute @a[hasitem={item=yes:oxidized_copper_leggings,location=slot.armor.legs,quantity=0},tag=wearing_oxidized_copper_leggings] ~~~tag @s remove wearing_oxidized_copper_leggings

execute @a[hasitem={item=yes:oxidized_copper_boots,location=slot.armor.feet},tag=!wearing_oxidized_copper_boots] ~~~ playsound armor.equip_iron @a[r=5] ~~1~ 100 0.9
execute @a[hasitem={item=yes:oxidized_copper_boots,location=slot.armor.feet},tag=!wearing_oxidized_copper_boots] ~~~ tag @s add wearing_oxidized_copper_boots
execute @a[hasitem={item=yes:oxidized_copper_boots,location=slot.armor.feet,quantity=0},tag=wearing_oxidized_copper_boots] ~~~tag @s remove wearing_oxidized_copper_boots