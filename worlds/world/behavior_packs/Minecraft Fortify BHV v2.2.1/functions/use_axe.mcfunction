summon yes:on_use_entity ~~~
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ log -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ log2 -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ warped_hyphae -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ warped_stem -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ crimson_hyphae -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ crimson_stem -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ mangrove_log -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ mangrove_wood -1 playsound hit.wood @p ~~1~ 1 0.9
execute @e[c=1,type=yes:on_use_entity] ~~~ detect ~~~ wood -1 playsound hit.wood @p ~~1~ 1 0.9

# =========

fill ~~~ ~~~ stripped_oak_log 0 replace log 0
fill ~~~ ~~~ stripped_oak_log 1 replace log 4
fill ~~~ ~~~ stripped_oak_log 2 replace log 8

fill ~~~ ~~~ stripped_spruce_log 0 replace log 1
fill ~~~ ~~~ stripped_spruce_log 1 replace log 5
fill ~~~ ~~~ stripped_spruce_log 2 replace log 9

fill ~~~ ~~~ stripped_birch_log 0 replace log 2
fill ~~~ ~~~ stripped_birch_log 1 replace log 6
fill ~~~ ~~~ stripped_birch_log 2 replace log 10

fill ~~~ ~~~ stripped_jungle_log 0 replace log 3
fill ~~~ ~~~ stripped_jungle_log 1 replace log 7
fill ~~~ ~~~ stripped_jungle_log 2 replace log 11

fill ~~~ ~~~ stripped_acacia_log 0 replace log2 0
fill ~~~ ~~~ stripped_acacia_log 1 replace log2 4
fill ~~~ ~~~ stripped_acacia_log 2 replace log2 8

fill ~~~ ~~~ stripped_dark_oak_log 0 replace log2 1
fill ~~~ ~~~ stripped_dark_oak_log 1 replace log2 5
fill ~~~ ~~~ stripped_dark_oak_log 2 replace log2 9

# =========

fill ~~~ ~~~ stripped_warped_hyphae 0 replace warped_hyphae 0
fill ~~~ ~~~ stripped_warped_hyphae 1 replace warped_hyphae 1
fill ~~~ ~~~ stripped_warped_hyphae 2 replace warped_hyphae 2
fill ~~~ ~~~ stripped_warped_stem 0 replace warped_stem 0
fill ~~~ ~~~ stripped_warped_stem 1 replace warped_stem 1
fill ~~~ ~~~ stripped_warped_stem 2 replace warped_stem 2
fill ~~~ ~~~ stripped_crimson_hyphae 0 replace crimson_hyphae 0
fill ~~~ ~~~ stripped_crimson_hyphae 1 replace crimson_hyphae 1
fill ~~~ ~~~ stripped_crimson_hyphae 2 replace crimson_hyphae 2
fill ~~~ ~~~ stripped_crimson_stem 0 replace crimson_stem 0
fill ~~~ ~~~ stripped_crimson_stem 1 replace crimson_stem 1
fill ~~~ ~~~ stripped_crimson_stem 2 replace crimson_stem 2

fill ~~~ ~~~ stripped_mangrove_log 0 replace mangrove_log 0
fill ~~~ ~~~ stripped_mangrove_log 1 replace mangrove_log 1
fill ~~~ ~~~ stripped_mangrove_log 2 replace mangrove_log 2
fill ~~~ ~~~ stripped_mangrove_wood 0 replace mangrove_wood 0
fill ~~~ ~~~ stripped_mangrove_wood 1 replace mangrove_wood 1
fill ~~~ ~~~ stripped_mangrove_wood 2 replace mangrove_wood 2

# =========

fill ~~~ ~~~ wood 8 replace wood 0
fill ~~~ ~~~ wood 9 replace wood 1
fill ~~~ ~~~ wood 10 replace wood 2
fill ~~~ ~~~ wood 11 replace wood 3
fill ~~~ ~~~ wood 12 replace wood 4
fill ~~~ ~~~ wood 13 replace wood 5

