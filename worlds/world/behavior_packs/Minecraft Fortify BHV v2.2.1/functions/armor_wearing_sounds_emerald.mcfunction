execute @a[hasitem={item=yes:emerald_helmet,location=slot.armor.head},tag=!wearing_emerald_helmet] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 0.8
execute @a[hasitem={item=yes:emerald_helmet,location=slot.armor.head},tag=!wearing_emerald_helmet] ~~~ tag @s add wearing_emerald_helmet
execute @a[hasitem={item=yes:emerald_helmet,location=slot.armor.head,quantity=0},tag=wearing_emerald_helmet] ~~~tag @s remove wearing_emerald_helmet

execute @a[hasitem={item=yes:emerald_chestplate,location=slot.armor.chest},tag=!wearing_emerald_chestplate] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 0.8
execute @a[hasitem={item=yes:emerald_chestplate,location=slot.armor.chest},tag=!wearing_emerald_chestplate] ~~~ tag @s add wearing_emerald_chestplate
execute @a[hasitem={item=yes:emerald_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_emerald_chestplate] ~~~tag @s remove wearing_emerald_chestplate

execute @a[hasitem={item=yes:emerald_leggings,location=slot.armor.legs},tag=!wearing_emerald_leggings] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 0.8
execute @a[hasitem={item=yes:emerald_leggings,location=slot.armor.legs},tag=!wearing_emerald_leggings] ~~~ tag @s add wearing_emerald_leggings
execute @a[hasitem={item=yes:emerald_leggings,location=slot.armor.legs,quantity=0},tag=wearing_emerald_leggings] ~~~tag @s remove wearing_emerald_leggings

execute @a[hasitem={item=yes:emerald_boots,location=slot.armor.feet},tag=!wearing_emerald_boots] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 0.8
execute @a[hasitem={item=yes:emerald_boots,location=slot.armor.feet},tag=!wearing_emerald_boots] ~~~ tag @s add wearing_emerald_boots
execute @a[hasitem={item=yes:emerald_boots,location=slot.armor.feet,quantity=0},tag=wearing_emerald_boots] ~~~tag @s remove wearing_emerald_boots

