scoreboard objectives add greatsword dummy

execute @a[scores={greatsword=1..}] ~~~ scoreboard players remove @s greatsword 1
execute @a[scores={greatsword=0}] ~~~ event entity @e[type=yes:player_look_entity,c=1,r=3.7] yes:despawn
execute @a[scores={greatsword=0}] ~~~ scoreboard players set @s greatsword -1

execute @a[hasitem={item=yes:aetherite_greatsword,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:aetherite_greatsword,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:aetherite_greatsword,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:aetherite_greatsword,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:aetherite_greatsword,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4

execute @a[hasitem={item=yes:emerald_greatsword,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:emerald_greatsword,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:emerald_greatsword,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:emerald_greatsword,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:emerald_greatsword,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4


execute @a[hasitem={item=yes:aetherite_greatsword_ad1,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:aetherite_greatsword_ad1,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:aetherite_greatsword_ad1,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:aetherite_greatsword_ad1,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:aetherite_greatsword_ad1,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4

execute @a[hasitem={item=yes:emerald_greatsword_ad1,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:emerald_greatsword_ad1,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:emerald_greatsword_ad1,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:emerald_greatsword_ad1,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:emerald_greatsword_ad1,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4


execute @a[hasitem={item=yes:aetherite_greatsword_ad2,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:aetherite_greatsword_ad2,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:aetherite_greatsword_ad2,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:aetherite_greatsword_ad2,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:aetherite_greatsword_ad2,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4

execute @a[hasitem={item=yes:emerald_greatsword_ad2,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:emerald_greatsword_ad2,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:emerald_greatsword_ad2,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:emerald_greatsword_ad2,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:emerald_greatsword_ad2,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4


execute @a[hasitem={item=yes:aetherite_greatsword_ad3,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:aetherite_greatsword_ad3,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:aetherite_greatsword_ad3,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:aetherite_greatsword_ad3,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:aetherite_greatsword_ad3,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4

execute @a[hasitem={item=yes:emerald_greatsword_ad3,location=slot.weapon.mainhand}] ~~~ scoreboard players add @s greatsword 0
execute @a[hasitem={item=yes:emerald_greatsword_ad3,location=slot.weapon.mainhand},scores={greatsword=!2..}] ~~~ scoreboard players set @s greatsword 1
execute @a[hasitem={item=yes:emerald_greatsword_ad3,location=slot.weapon.mainhand},scores={greatsword=1}] ~~~ ride @e[type=yes:player_entity,c=1,r=3.7] summon_rider yes:player_look_entity
execute @a[tag=!dead,hasitem={item=yes:emerald_greatsword_ad3,location=slot.weapon.mainhand}] ~~~ execute @e[type=yes:player_look_entity,c=1,r=3.7] ~~~ execute @a[c=1,r=3.7,tag=!dead,hasitem={item=yes:emerald_greatsword_ad3,location=slot.weapon.mainhand}] ~~~ scoreboard players set @s greatsword 4



scoreboard players remove @a[scores={greatsword_atk=1..}] greatsword_atk 1
execute @a[tag=using_greatsword,scores={greatsword_atk=0}] ~~~ scoreboard players set @s greatsword_anim 0
execute @a[tag=using_greatsword,scores={greatsword_atk=0}] ~~~ scoreboard players set @s gs_durability 0
execute @a[tag=using_greatsword,scores={greatsword_atk=0}] ~~~ tag @s remove using_greatsword
execute @a[tag=greatsword_loop,scores={greatsword_atk=0}] ~~~ scoreboard objectives setdisplay list
execute @a[tag=greatsword_loop,scores={greatsword_atk=0}] ~~~ tag @s remove greatsword_loop
execute @a[scores={greatsword_atk=0}] ~~~ scoreboard players set @s greatsword_atk -1

execute @a[scores={greatsword=1..}] ~~~ scoreboard players remove @s greatsword 1
execute @a[scores={greatsword=0}] ~~~ event entity @e[type=yes:player_look_entity,c=1,r=3.7] yes:despawn
execute @a[scores={greatsword=0}] ~~~ scoreboard players set @s greatsword -1