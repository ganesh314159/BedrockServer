scoreboard objectives add aetherium_burn dummy

execute @e[type=item,tag=aetherium_crystal] ~~~ detect ~~~ lava_cauldron -1 scoreboard players add @s aetherium_burn 1
execute @e[type=item,tag=aetherium_crystal] ~~~ detect ~~~ lava_cauldron -1 playsound random.fizz @a[r=10] ~~1~ 0.8 1.2 0.3
execute @e[type=item,tag=aetherium_crystal] ~~~ detect ~~~ lava_cauldron -1 particle minecraft:lava_particle ~~0.5~

execute @e[type=item,tag=aetherium_crystal,scores={aetherium_burn=40..}] ~~~ setblock ~~~ cauldron
execute @e[type=item,tag=aetherium_crystal,scores={aetherium_burn=40..}] ~~~ summon yes:aetherite_ingot_transform ~~~
execute @e[type=item,tag=aetherium_crystal,scores={aetherium_burn=40..}] ~~~ scoreboard players set @s aetherium_burn 0