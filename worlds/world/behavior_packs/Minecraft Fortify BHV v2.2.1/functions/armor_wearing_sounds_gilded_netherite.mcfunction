execute @a[hasitem={item=yes:gilded_netherite_helmet,location=slot.armor.head},tag=!wearing_gilded_netherite_helmet] ~~~ playsound armor.equip_netherite @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:gilded_netherite_helmet,location=slot.armor.head},tag=!wearing_gilded_netherite_helmet] ~~~ tag @s add wearing_gilded_netherite_helmet
execute @a[hasitem={item=yes:gilded_netherite_helmet,location=slot.armor.head,quantity=0},tag=wearing_gilded_netherite_helmet] ~~~tag @s remove wearing_gilded_netherite_helmet

execute @a[hasitem={item=yes:gilded_netherite_chestplate,location=slot.armor.chest},tag=!wearing_gilded_netherite_chestplate] ~~~ playsound armor.equip_netherite @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:gilded_netherite_chestplate,location=slot.armor.chest},tag=!wearing_gilded_netherite_chestplate] ~~~ tag @s add wearing_gilded_netherite_chestplate
execute @a[hasitem={item=yes:gilded_netherite_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_gilded_netherite_chestplate] ~~~tag @s remove wearing_gilded_netherite_chestplate

execute @a[hasitem={item=yes:gilded_netherite_leggings,location=slot.armor.legs},tag=!wearing_gilded_netherite_leggings] ~~~ playsound armor.equip_netherite @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:gilded_netherite_leggings,location=slot.armor.legs},tag=!wearing_gilded_netherite_leggings] ~~~ tag @s add wearing_gilded_netherite_leggings
execute @a[hasitem={item=yes:gilded_netherite_leggings,location=slot.armor.legs,quantity=0},tag=wearing_gilded_netherite_leggings] ~~~tag @s remove wearing_gilded_netherite_leggings

execute @a[hasitem={item=yes:gilded_netherite_boots,location=slot.armor.feet},tag=!wearing_gilded_netherite_boots] ~~~ playsound armor.equip_netherite @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:gilded_netherite_boots,location=slot.armor.feet},tag=!wearing_gilded_netherite_boots] ~~~ tag @s add wearing_gilded_netherite_boots
execute @a[hasitem={item=yes:gilded_netherite_boots,location=slot.armor.feet,quantity=0},tag=wearing_gilded_netherite_boots] ~~~tag @s remove wearing_gilded_netherite_boots

