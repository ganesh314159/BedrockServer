execute @a[hasitem={item=yes:glowing_obsidian_helmet,location=slot.armor.head},tag=!wearing_glowing_obsidian_helmet] ~~~ playsound armor.equip_chain @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:glowing_obsidian_helmet,location=slot.armor.head},tag=!wearing_glowing_obsidian_helmet] ~~~ tag @s add wearing_glowing_obsidian_helmet
execute @a[hasitem={item=yes:glowing_obsidian_helmet,location=slot.armor.head,quantity=0},tag=wearing_glowing_obsidian_helmet] ~~~tag @s remove wearing_glowing_obsidian_helmet

execute @a[hasitem={item=yes:glowing_obsidian_chestplate,location=slot.armor.chest},tag=!wearing_glowing_obsidian_chestplate] ~~~ playsound armor.equip_chain @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:glowing_obsidian_chestplate,location=slot.armor.chest},tag=!wearing_glowing_obsidian_chestplate] ~~~ tag @s add wearing_glowing_obsidian_chestplate
execute @a[hasitem={item=yes:glowing_obsidian_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_glowing_obsidian_chestplate] ~~~tag @s remove wearing_glowing_obsidian_chestplate

execute @a[hasitem={item=yes:glowing_obsidian_leggings,location=slot.armor.legs},tag=!wearing_glowing_obsidian_leggings] ~~~ playsound armor.equip_chain @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:glowing_obsidian_leggings,location=slot.armor.legs},tag=!wearing_glowing_obsidian_leggings] ~~~ tag @s add wearing_glowing_obsidian_leggings
execute @a[hasitem={item=yes:glowing_obsidian_leggings,location=slot.armor.legs,quantity=0},tag=wearing_glowing_obsidian_leggings] ~~~tag @s remove wearing_glowing_obsidian_leggings

execute @a[hasitem={item=yes:glowing_obsidian_boots,location=slot.armor.feet},tag=!wearing_glowing_obsidian_boots] ~~~ playsound armor.equip_chain @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:glowing_obsidian_boots,location=slot.armor.feet},tag=!wearing_glowing_obsidian_boots] ~~~ tag @s add wearing_glowing_obsidian_boots
execute @a[hasitem={item=yes:glowing_obsidian_boots,location=slot.armor.feet,quantity=0},tag=wearing_glowing_obsidian_boots] ~~~tag @s remove wearing_glowing_obsidian_boots