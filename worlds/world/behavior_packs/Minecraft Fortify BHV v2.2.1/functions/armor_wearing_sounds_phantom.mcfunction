execute @a[hasitem={item=yes:phantom_helmet,location=slot.armor.head},tag=!wearing_phantom_helmet] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:phantom_helmet,location=slot.armor.head},tag=!wearing_phantom_helmet] ~~~ tag @s add wearing_phantom_helmet
execute @a[hasitem={item=yes:phantom_helmet,location=slot.armor.head,quantity=0},tag=wearing_phantom_helmet] ~~~tag @s remove wearing_phantom_helmet

execute @a[hasitem={item=yes:phantom_chestplate,location=slot.armor.chest},tag=!wearing_phantom_chestplate] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:phantom_chestplate,location=slot.armor.chest},tag=!wearing_phantom_chestplate] ~~~ tag @s add wearing_phantom_chestplate
execute @a[hasitem={item=yes:phantom_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_phantom_chestplate] ~~~tag @s remove wearing_phantom_chestplate

execute @a[hasitem={item=yes:phantom_leggings,location=slot.armor.legs},tag=!wearing_phantom_leggings] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:phantom_leggings,location=slot.armor.legs},tag=!wearing_phantom_leggings] ~~~ tag @s add wearing_phantom_leggings
execute @a[hasitem={item=yes:phantom_leggings,location=slot.armor.legs,quantity=0},tag=wearing_phantom_leggings] ~~~tag @s remove wearing_phantom_leggings

execute @a[hasitem={item=yes:phantom_boots,location=slot.armor.feet},tag=!wearing_phantom_boots] ~~~ playsound armor.equip_leather @a[r=5] ~~1~ 100 0.75
execute @a[hasitem={item=yes:phantom_boots,location=slot.armor.feet},tag=!wearing_phantom_boots] ~~~ tag @s add wearing_phantom_boots
execute @a[hasitem={item=yes:phantom_boots,location=slot.armor.feet,quantity=0},tag=wearing_phantom_boots] ~~~tag @s remove wearing_phantom_boots

