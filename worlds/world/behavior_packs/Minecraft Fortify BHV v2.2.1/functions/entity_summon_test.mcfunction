scoreboard objectives add playerentity dummy
scoreboard objectives add pecheck dummy

# =============== testfor players ===================
execute @e[type=yes:player_entity] ~~~ scoreboard players remove @s[scores={pecheck=1..}] pecheck 1
execute @e[type=yes:player_entity] ~~~ execute @e[type=player,r=5] ~~~ scoreboard players set @e[type=yes:player_entity,r=5] pecheck 1
execute @e[type=yes:player_entity,scores={pecheck=0}] ~~~ event entity @s yes:despawn
execute @e[type=yes:player_look_entity] ~~~ scoreboard players remove @s[scores={pecheck=1..}] pecheck 1
execute @e[type=yes:player_look_entity] ~~~ execute @e[type=player,r=3.5] ~~~ scoreboard players set @e[type=yes:player_look_entity,r=3.7] pecheck 1
execute @e[type=yes:player_look_entity,scores={pecheck=0}] ~~~ event entity @s yes:despawn



scoreboard players set @a[scores={playerentity=!200..-200}] pecheck 1
scoreboard players add @a playerentity 1
scoreboard players set @a playerentityWard 0
scoreboard players set @a playerentityPhan 0
scoreboard players set @a playerentityGlow 0
scoreboard players set @a playerentityGild 0
scoreboard players set @a playerentityCopp 0

execute @a[scores={playerentity=50..200}] ~~~ function entity_summon_check_test


scoreboard objectives add weapon_sound dummy

# ===== Gilded Netherite Armor setup =====

scoreboard objectives add gilded_health dummy
scoreboard players add @a gilded_health 0


# ===== Phantom Armor setup =====

execute @a[scores={phantom_invis=3..200,phantom_check=1..30}] ~~~ scoreboard players remove @s phantom_check 1
execute @e[type=yes:player_entity,scores={is_sneaking=1..4}] ~~~ scoreboard players remove @s is_sneaking 1
execute @e[type=yes:player_entity,scores={phantom_check2=1..30}] ~~~ scoreboard players remove @s phantom_check2 1

execute @a[scores={phantom_check=0}] ~~~ scoreboard players set @s phantom_invis 0
effect @a[scores={phantom_check=0}] invisibility 0 0 true
scoreboard players set @a[scores={phantom_check=0}] phantom_check -1

# ===== Warden Armor setup =====

scoreboard objectives add warden_heartbeat dummy
scoreboard players add @a warden_heartbeat 0

scoreboard players add @a[scores={warden_heartbeat=0..32}] warden_heartbeat 1
execute @a[tag=warden_armor_stage1,hasitem={item=yes:warden_helmet,location=slot.armor.head},scores={warden_heartbeat=33}] ~~~ execute @s[hasitem={item=yes:warden_chestplate,location=slot.armor.chest}] ~~~ execute @s[hasitem={item=yes:warden_leggings,location=slot.armor.legs}] ~~~ execute @s[hasitem={item=yes:warden_boots,location=slot.armor.feet}] ~~~ playsound mob.warden.heartbeat.weewee @s ~~1~ 0.4
execute @a[tag=warden_armor_stage2,hasitem={item=yes:warden_helmet,location=slot.armor.head},scores={warden_heartbeat=33}] ~~~ execute @s[hasitem={item=yes:warden_chestplate,location=slot.armor.chest}] ~~~ execute @s[hasitem={item=yes:warden_leggings,location=slot.armor.legs}] ~~~ execute @s[hasitem={item=yes:warden_boots,location=slot.armor.feet}] ~~~ playsound mob.warden.heartbeat.weewee @s ~~1~ 0.2
execute @a[scores={warden_heartbeat=33}] ~~~ scoreboard players set @s warden_heartbeat 0
