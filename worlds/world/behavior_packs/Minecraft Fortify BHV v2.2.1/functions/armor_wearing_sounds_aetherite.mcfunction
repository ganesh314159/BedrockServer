execute @a[hasitem={item=yes:aetherite_helmet,location=slot.armor.head},tag=!wearing_aetherite_helmet] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.3
execute @a[hasitem={item=yes:aetherite_helmet,location=slot.armor.head},tag=!wearing_aetherite_helmet] ~~~ playsound break.large_amethyst_bud @a[r=5] ~~1~ 0.4 1.3
execute @a[hasitem={item=yes:aetherite_helmet,location=slot.armor.head},tag=!wearing_aetherite_helmet] ~~~ tag @s add wearing_aetherite_helmet
execute @a[hasitem={item=yes:aetherite_helmet,location=slot.armor.head,quantity=0},tag=wearing_aetherite_helmet] ~~~tag @s remove wearing_aetherite_helmet

execute @a[hasitem={item=yes:aetherite_chestplate,location=slot.armor.chest},tag=!wearing_aetherite_chestplate] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.3
execute @a[hasitem={item=yes:aetherite_chestplate,location=slot.armor.chest},tag=!wearing_aetherite_chestplate] ~~~ playsound break.large_amethyst_bud @a[r=5] ~~1~ 0.4 1.1

execute @a[hasitem={item=yes:aetherite_chestplate,location=slot.armor.chest},tag=!wearing_aetherite_chestplate] ~~~ tag @s add wearing_aetherite_chestplate
execute @a[hasitem={item=yes:aetherite_chestplate,location=slot.armor.chest,quantity=0},tag=wearing_aetherite_chestplate] ~~~tag @s remove wearing_aetherite_chestplate

execute @a[hasitem={item=yes:aetherite_leggings,location=slot.armor.legs},tag=!wearing_aetherite_leggings] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.3
execute @a[hasitem={item=yes:aetherite_leggings,location=slot.armor.legs},tag=!wearing_aetherite_leggings] ~~~ playsound break.large_amethyst_bud @a[r=5] ~~1~ 0.4 1.2
execute @a[hasitem={item=yes:aetherite_leggings,location=slot.armor.legs},tag=!wearing_aetherite_leggings] ~~~ tag @s add wearing_aetherite_leggings
execute @a[hasitem={item=yes:aetherite_leggings,location=slot.armor.legs,quantity=0},tag=wearing_aetherite_leggings] ~~~tag @s remove wearing_aetherite_leggings

execute @a[hasitem={item=yes:aetherite_boots,location=slot.armor.feet},tag=!wearing_aetherite_boots] ~~~ playsound armor.equip_diamond @a[r=5] ~~1~ 100 1.3
execute @a[hasitem={item=yes:aetherite_boots,location=slot.armor.feet},tag=!wearing_aetherite_boots] ~~~ playsound break.large_amethyst_bud @a[r=5] ~~1~ 0.4 1.3
execute @a[hasitem={item=yes:aetherite_boots,location=slot.armor.feet},tag=!wearing_aetherite_boots] ~~~ tag @s add wearing_aetherite_boots
execute @a[hasitem={item=yes:aetherite_boots,location=slot.armor.feet,quantity=0},tag=wearing_aetherite_boots] ~~~tag @s remove wearing_aetherite_boots

