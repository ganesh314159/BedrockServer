/**
 * @license MIT
 * @author JaylyMC
 */
 
/**
 * Modified version to work with 1.19.40
 */
import { system, EntityHealthComponent, world, Player } from "@minecraft/server";

/**
 * Contains information related to an player death.
 */
export class PlayerDeathEvent {
  /**
   * @param {Player} player 
   */
  constructor (player) {
    this.player = player;
  };
};

/**
 * Manages callbacks that are connected to when an player dies.
 */
export class PlayerDeathEventSignal {
  /**
   * Subscribe
   * @param {(arg: PlayerDeathEvent) => void} arg 
   * @return {(arg: PlayerDeathEvent) => void}
   */
  subscribe (arg) {
    arg["playerDeath"] = true;
    /**
     * @type {Player[]}
     */
    const deadPlayers = [];
    system.run(function callback() => {
	  if(Array.from(world.getPlayers()).length<=0) system.run(callback);
      for (let player of Array.from(world.getPlayers())) {
        if (!player.hasComponent("health")){
			system.run(callback);
			return;
		}
        /**
         * @type {EntityHealthComponent}
         */
        // @ts-ignore
        let health = player.getComponent("health");
        if (health.current === 0 && arg["playerDeath"] === true) {
		  system.run(callback);
          const playerIndex = deadPlayers.findIndex(pl => pl.name === player.name);
          if (playerIndex < 0) {
            arg(new PlayerDeathEvent(player));
            deadPlayers.push(player);
            system.run(function playerDeathCallback() => {
              if (health.current > 0) {
                deadPlayers.splice(playerIndex, 1);                
              }else{
				system.run(playerDeathCallback);
			  }
            });
          }      
        } else if (arg["playerDeath"] === false) {
          
        }else{
			system.run(callback);
		}
      }
    });

    return arg;
  };

  /**
   * Unsubscribe
   * @param {(arg: PlayerDeathEvent) => void} arg 
   * @return {void}
   */
  unsubscribe (arg) {
    arg["playerDeath"] = false;
  };
};