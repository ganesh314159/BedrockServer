	import { world, system, ItemTypes, ItemStack, MinecraftBlockTypes} from '@minecraft/server';

	let playerDeathLocations = [];
	let playersDeathLocations = [];
	let playerInventories = {};
	
		system.runInterval(function tick(){
			let players = Array.from(world.getPlayers());
			for(let player of players){
				
				//Give key when player respawn
				let currentHealth = player.getComponent(`health`).current;
				let deathTag = player.hasTag('death_key');
				if(currentHealth > 0 && deathTag){
					player.removeTag('death_key');
					giveKey(player);
				}
				
				//For instant teleport key
				let key = player.getComponent('inventory').container.getItem(player.selectedSlot);
				if(!key) continue;
				let itemHolding = key.typeId;
				if(itemHolding == 'graves:grave_key_teleport'){
				let locTag = player.getTags().find(tag=>tag.includes('Graves'));
				if(!locTag){			
					continue;
				}
				let arrTag = locTag.replace('Graves','').split(',');
				let loc = {x:parseInt(arrTag[0]),y:parseInt(arrTag[1]),z:parseInt(arrTag[2])};
				key.setLore([`Death Location:`,`${arrTag[3]}`,`x:${Math.floor(loc.x)}, y:${Math.floor(loc.y)}, z:${Math.floor(loc.z)}`]);
				player.getComponent('inventory').container.setItem(player.selectedSlot,key);
				}
			}
			
		},10);

		//Getting player inventory per tick
		/*
		system.runInterval(()=>{
			let allPlayers = Array.from(world.getPlayers());
			for (let player of allPlayers){
				let health = player.getComponent("health").current;
				if(health <= 0) continue;
				let name = player.name;
				let getPlayerInventory = player.getComponent("inventory").container;
				let playerInventory = []
				for(let index=0; index<getPlayerInventory.size; index++){
					if(!getPlayerInventory.getItem(index)) continue;
					playerInventory.push(getPlayerInventory.getItem(index))
				}					
				playerInventories[`${name}`] = playerInventory;
			}
		},1);*/

	function giveKey(player){
		//Get before death location
		let name = player.name;
		let locArr = playersDeathLocations[name][0];
		let loc = {x:parseInt(locArr[0]),y:parseInt(locArr[1]),z:parseInt(locArr[2])};
		//--
		let playerInv = player.getComponent(`inventory`).container;
		let key = new ItemStack(ItemTypes.get('graves:grave_key'),1);
		key.setLore([`Death Location:`,`${locArr[3]}`,`x:${Math.floor(loc.x)}, y:${Math.floor(loc.y)}, z:${Math.floor(loc.z)}`]);
		playerInv.addItem(key);
	}

	world.events.entityDie.subscribe((eventData) => {
	  const player = eventData.deadEntity;
	  const name = player.name;
	  if(!player.typeId.includes("player")) return;
	  const blockLoc = {x:Math.floor(player.location.x),y:Math.floor(player.location.y),z:Math.floor(player.location.z)};
	  const entityLoc = {x:Math.floor(player.location.x)+0.5,y:Math.floor(player.location.y),z:Math.floor(player.location.z)+0.5};
	  const dimension = player.dimension;
	  const graveEntity = dimension.spawnEntity("grave:grave_dirt",entityLoc);
	  graveEntity.nameTag = name + "'s Grave";
	  graveEntity.triggerEvent("asyphon");
	  graveEntity.triggerEvent("grave");
	  dimension.fillBlocks(blockLoc,blockLoc,MinecraftBlockTypes.get("grave:grave"));
	  player.addTag('death_key');	  
	  for(let tag of getGraveTags(player.getTags())){
		  player.removeTag(tag);
	  }
	  player.addTag(`Graves${Math.floor(blockLoc.x)},${Math.floor(blockLoc.y)},${Math.floor(blockLoc.z)},${dimension.id}`);
	  if(!playersDeathLocations.hasOwnProperty(name)){
		playersDeathLocations[name] = [];
	  }	  
	  playersDeathLocations[name].push(`${Math.floor(blockLoc.x)}, ${Math.floor(blockLoc.y)}, ${Math.floor(blockLoc.z)}, ${dimension}`);
	});

	world.events.beforeItemUse.subscribe((eventData)=>{
		let player = eventData.source;
		let itemUsing = player.getComponent('inventory').container.getItem(player.selectedSlot).typeId;
		if(itemUsing == 'graves:grave_key_teleport'){
			let locTag = player.getTags().find(tag=>tag.includes('Graves'));
			if(!locTag){
				player.onScreenDisplay.setActionBar("§cNo Previous Graves Found!");
				return;
			}
			let arrTag = locTag.replace('Graves','').split(',');
			let loc = {x:parseInt(arrTag[0]),y:parseInt(arrTag[1]),z:parseInt(arrTag[2])};
			player.teleport(loc,world.getDimension(arrTag[3]),0,0,false);
			player.removeTag(locTag);
		}
	});

	world.events.beforeItemUseOn.subscribe((eventData) =>{
		let player = eventData.source;
		let keyItem = eventData.item.typeId;		
		if(keyItem == "graves:grave_key"){					
			let tags = getGraveTags(player.getTags())[0];
			if(tags){
				if(player.hasTag(tags)){
					player.removeTag(tags);		
				}
			}
		}
	});
	
	function getGraveTags(playerTags){
		let filterTags = playerTags.filter((tag)=>{
			return tag.includes("Graves");
		});
		if(filterTags.length>0){	
			return filterTags;
		}
		return "";
	}
	
	function getGraveCoords(graveTags){
		let coordinateString = graveTags.replace("Graves","").replace("minecraft:overworld", "").split(",");
		let loc = {x:parseInt(coordinateString[0]),y:parseInt(coordinateString[1]),z:parseInt(coordinateString[2])};
		return loc;
	}