gamerule sendcommandfeedback false

execute @a [hasitem={location=slot.armor.legs,item=btr:agility_i_iron_leggings}] ~ ~ ~ scoreboard players add @s speedeffect 1
execute @a [hasitem={location=slot.armor.legs,item=btr:agility_ii_iron_leggings}] ~ ~ ~ scoreboard players add @s speedeffect 2
execute @a [hasitem={location=slot.armor.legs,item=btr:agility_i_diamond_leggings}] ~ ~ ~ scoreboard players add @s speedeffect 1
execute @a [hasitem={location=slot.armor.legs,item=btr:agility_ii_diamond_leggings}] ~ ~ ~ scoreboard players add @s speedeffect 2
execute @a [hasitem={location=slot.armor.legs,item=btr:agility_i_netherite_leggings}] ~ ~ ~ scoreboard players add @s speedeffect 1
execute @a [hasitem={location=slot.armor.legs,item=btr:agility_ii_netherite_leggings}] ~ ~ ~ scoreboard players add @s speedeffect 2

execute @a [hasitem={location=slot.armor.chest,item=btr:vitality_iron_chestplate}] ~ ~ ~ scoreboard players add @s regeneffect 1
execute @a [hasitem={location=slot.armor.chest,item=btr:vitality_diamond_chestplate}] ~ ~ ~ scoreboard players add @s regeneffect 1
execute @a [hasitem={location=slot.armor.chest,item=btr:vitality_netherite_chestplate}] ~ ~ ~ scoreboard players add @s regeneffect 1

execute @a [hasitem={location=slot.armor.feet,item=btr:high_jump_i_iron_boots}] ~ ~ ~ scoreboard players add @s jump_boosteffect 3
execute @a [hasitem={location=slot.armor.feet,item=btr:high_jump_ii_iron_boots}] ~ ~ ~ scoreboard players add @s jump_boosteffect 5
execute @a [hasitem={location=slot.armor.feet,item=btr:high_jump_i_diamond_boots}] ~ ~ ~ scoreboard players add @s jump_boosteffect 3
execute @a [hasitem={location=slot.armor.feet,item=btr:high_jump_ii_diamond_boots}] ~ ~ ~ scoreboard players add @s jump_boosteffect 5
execute @a [hasitem={location=slot.armor.feet,item=btr:high_jump_i_netherite_boots}] ~ ~ ~ scoreboard players add @s jump_boosteffect 3
execute @a [hasitem={location=slot.armor.feet,item=btr:high_jump_ii_netherite_boots}] ~ ~ ~ scoreboard players add @s jump_boosteffect 5



execute @e[type=item,name="raw iron",tag=!clean] ~ ~ ~ execute @r[type=item,name="iron ingot",r=2] ~ ~ ~ kill @r[type=item,name="raw iron",r=2,tag=!clean]
tag @e[type=item,name="raw iron",tag=!clean] add clean
execute @e[type=item,name="raw gold",tag=!clean] ~ ~ ~ execute @r[type=item,name="gold ingot",r=2] ~ ~ ~ kill @r[type=item,name="raw gold",r=2,tag=!clean]
tag @e[type=item,name="raw gold",tag=!clean] add clean
execute @e[type=item,name="ancient debris",tag=!clean] ~ ~ ~ execute @r[type=item,name="netherite scrap",r=2] ~ ~ ~ kill @r[type=item,name="ancient debris",r=2,tag=!clean]
tag @e[type=item,name="ancient debris",tag=!clean] add clean

execute @e[type=item,name="coal",tag=!clean] ~ ~ ~ execute @r[type=item,name="diamond",r=2] ~ ~ ~ kill @r[type=item,name="coal",r=2,tag=!clean]
tag @e[type=item,name="coal",tag=!clean] add clean
execute @e[type=item,name="raw iron",tag=!clean] ~ ~ ~ execute @r[type=item,name="diamond",r=2] ~ ~ ~ kill @r[type=item,name="raw iron",r=2,tag=!clean]
tag @e[type=item,name="raw iron",tag=!clean] add clean
execute @e[type=item,name="raw gold",tag=!clean] ~ ~ ~ execute @r[type=item,name="diamond",r=2] ~ ~ ~ kill @r[type=item,name="raw gold",r=2,tag=!clean]
tag @e[type=item,name="raw gold",tag=!clean] add clean
execute @e[type=item,name="emerald",tag=!clean] ~ ~ ~ execute @r[type=item,name="diamond",r=2] ~ ~ ~ kill @r[type=item,name="emerald",r=2,tag=!clean]
tag @e[type=item,name="emerald",tag=!clean] add clean