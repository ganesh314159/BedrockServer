gamerule sendcommandfeedback false

tag @s add stunner
playsound random.anvil_land @a [r=10] ~~~ 0.5
effect @e [type=!item,tag=!stunner,c=1,r=5] slowness 3 255 true
tag @s remove stunner