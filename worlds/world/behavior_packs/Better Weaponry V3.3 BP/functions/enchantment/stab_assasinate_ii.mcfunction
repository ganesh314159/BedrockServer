gamerule sendcommandfeedback false

tag @s add stabber
playsound mob.bat.takeoff @a [r=10] ~~~ 0.5
execute @e [type=!item,tag=!stabber,c=1,r=5] ~ ~ ~ tp @e [tag=stabber,c=1,r=5] ^ ^ ^-2 facing @s true
execute @e [type=!item,tag=!stabber,c=1,r=5] ~ ~ ~ effect @e [tag=stabber,c=1,r=5] slowness 2 1 true
execute @e [type=!item,tag=!stabber,c=1,r=5] ~ ~ ~ effect @e [tag=stabber,c=1,r=5] strength 1 1 true
tag @s remove stabber