gamerule sendcommandfeedback false

tag @s add disarmer
playsound random.break @a [r=10] ~~~ 0.5
effect @e [type=!item,tag=!disarmer,c=1,r=5] mining_fatigue 3 255 true
effect @e [type=!item,tag=!disarmer,c=1,r=5] weakness 3 255 true
tag @s remove disarmer