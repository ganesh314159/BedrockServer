execute if entity @s[scores={Balance=0}] run titleraw @s actionbar {"rawtext":[{"text":"§eNothing was removed! "},{"text":"Your current Balance: "},{"text":"§6"},{"score":{"name":"@s","objective":"Balance"}}]}
execute if entity @s[scores={Balance=1..7}] run function mm_remove_coin
execute if entity @s[scores={Balance=8..31}] run function mm_remove_coin_stack
execute if entity @s[scores={Balance=32..63}] run function mm_remove_coin_block
execute if entity @s[scores={Balance=64..511}] run function mm_remove_cash
execute if entity @s[scores={Balance=512..2047}] run function mm_remove_cash_stack
execute if entity @s[scores={Balance=2048..}] run function mm_remove_cash_block