scoreboard objectives setdisplay sidebar
scoreboard objectives setdisplay list
scoreboard objectives setdisplay belowname
scoreboard players add @s TogBal 1
execute at @s[scores={TogBal=1}] run scoreboard objectives setdisplay list Balance
titleraw @s[scores={TogBal=1}] actionbar {"rawtext":[{"text":"§6Balance Display set to"},{"text":"§e List"},{"text":"§6!"}]}
playsound random.orb @s[scores={TogBal=1}] ~~~ 1 1 0
execute at @s[scores={TogBal=2}] run scoreboard objectives setdisplay belowname Balance
titleraw @s[scores={TogBal=2}] actionbar {"rawtext":[{"text":"§6Balance Display set to"},{"text":"§e Below Name"},{"text":"§6!"}]}
playsound random.orb @s[scores={TogBal=2}] ~~~ 1 1 0
execute at @s[scores={TogBal=3}] run scoreboard objectives setdisplay sidebar Balance
titleraw @s[scores={TogBal=3}] actionbar {"rawtext":[{"text":"§6Balance Display set to"},{"text":"§e Sidebar"},{"text":"§6!"}]}
playsound random.orb @s[scores={TogBal=3}] ~~~ 1 1 0
titleraw @s[scores={TogBal=4}] actionbar {"rawtext":[{"text":"§6Balance Display"},{"text":"§c Disabled"},{"text":"§6!"}]}
playsound random.orb @s[scores={TogBal=4}] ~~~ 1 1 0
scoreboard players set @s[scores={TogBal=4}] TogBal 0