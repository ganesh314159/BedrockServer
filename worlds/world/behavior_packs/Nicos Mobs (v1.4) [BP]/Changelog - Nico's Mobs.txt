Nico's Mobs

[Version: 1.4]
          "New Additional"
MOBS:
• Added Enchanted Book (Cursed Book variant)
BLOCK:
• Added Mud Soil Bricks Slab
• Added Mud Soil Bricks Stairs
ITEMS:
• Added Fried Moa Egg
• Added Moa Egg
• Added Sparrow Egg
RECIPES:
• Added 2 new Pumpkin Pie recipe.
• Added 2 new Cake recipe.

          "Changes & Fixes"
GENERAL:
• Updated for compatibility with Minecraft version 1.19.70.
BLOCKS:
• Increased the block friction of the custom blocks (the speed when you running or walk at the top of the block), to match the friction speed of the Vanilla's block.
MOBS:
• Hostile mobs will no longer target your passive tamed mobs.
• Seated golems are now immovable.
• Brown & Red Shroom - animation has been improved.
• Brown & Red Shroom - wild shrooms will take a nap at day time.
• Brown & Red Shroom - can now be leashed.
• Brown & Red Shroom - tamed shrooms will now spawn a small mushroom randomly.
• Brown & Red Shroom - fixed the untamed new born baby Shroom for being tamed.
• Brown & Red Shroom - retextured to spot the different between untamed and tamed shroom.
• Cave Dweller - animation has been improved.
• Cave Dweller - are now able to use a Bow, Trident, and Crossbow if they have it.
• Cave Dweller - can now pickup item and use it as their weapon.
• Cave Dweller - can now break doors.
• Cave Dweller - has now a small chance to be spawned without equipment or with a bow equipped.
• Cave Dweller - health value is decreased.
• Cave Dweller - texture has been improved.
• Cave Dweller - will now avoid the Warden.
• Cave Dweller - will no longer drop Stick.
• Cave Dweller - will no longer route on sculk blocks.
• Cave Wolf - animation has been improved.
• Cave Wolf - can now breed using any type of meat except fish meat.
• Cave Wolf - can now only be tamed using a bone. 
• Cave Wolf - health values are decreased.
• Cave Wolf - mount speed is decreased.
• Cave Wolf - tamed Cave Wolf will no longer target your other tamed mob once you attack it.
• Cave Wolf - untamed Cave Wolf will no longer route on sculk blocks.
• Cave Wolf - fixed the baby Cave Wolf behavior priority.
• Clurve - will no longer trigger the sculk blocks.
• Cobblestone Golem - attack damage is decreased.
• Cobblestone Golem - health value is decreased.
• Cobblestone Golem - untamed golem may now despawn from distance.
• Cobblestone Golem - reduced the knockback resistance.
• Cobblestone Golem - texture is now emissive.
• Cursed Book - now emits particles.
• Cursed Book - texture has been improved.
• Cursed Book - will now disappear after a few minutes.
• Deepslate Golem - health value is increased.
• Deepslate Golem - reduced the mass amount from being seated and standing.
• Dire Bat - can now be leashed.
• Enchanter Apprentice - can now summon the Enchanted Book along with Cursed Book.
• Enchanter Apprentice - health value is decreased.
• Enchanter Apprentice - may now despawn from distance.
• Enchanter Apprentice - movement value is decreased.
• Enchanter Apprentice - texture is now emissive.
• Enchanter Apprentice - will no longer get triggered by other illager's attack.
• Enchanter Apprentice - will no longer route on sculk blocks.
• Forest Guardian - can now naturally target all the Illager mobs, including Cursed Book and Vex.
• Forest Guardian - may now despawn from distance.
• Forest Guardian - texture is now emissive.
• Forest Wisp - will no longer spawn in Snowy Taiga.
• Giant Moa - health values are decreased.
• Giant Moa - will now laid egg after they breed.
• Giant Moa - will now sleep at night time.
• Giant Moa - animation has been improved.
• Giant Moa - are now also aggressively towards the Endermite.
• Giant Moa - fixed the untamed new born baby Moa for being tamed.
• Giant Moa - baby moa attack damage is decreased.
• Giant Moa - tamed moa will no longer despawn or disappear from the distance.
• Giant Moa - texture and model has been improved.
• Giant Moa - will no longer be healed & aged by using a Beetroot.
• Giant Moa - will no longer be spawned on Roofed Forest.
• Jellyfish - they can now obtain using a Water Bucket instead of Empty Bucket.
• Jellyfish - will no longer be leashed.
• Magma Golem - attack damage is increased.
• Magma Golem - health value is increased.
• Mossy Cobblestone Golem - attack damage is decreased.
• Mossy Cobblestone Golem - texture is now emissive.
• Prismarine Clam - they can now obtain using a Water Bucket instead of Empty Bucket.
• Prismarine Clam - changed the interaction sound.
• Skeleton Warriors - animation has been improved.
• Skeleton Warriors - are now able to use a Bow, Trident, and Crossbow if they have it.
• Skeleton Warriors - can now pickup item and use it as their weapon.
• Skeleton Warriors - will now burn on daylight.
• Skeleton Warriors - will now avoid the Sculk Sensor & Sculk Shrieker.
• Sparrow - are now breedable using any type of seed.
• Sparrow - are now healable by feeding them any type of seed.
• Sparrow - will now spawn a Sparrow Egg.
• Squirrel - fixed the breeding of Squirrel.
• Squirrel - increased the scale size of baby Squirrel.
• Wandering Collectors - will now avoid and will no longer route on sculk blocks.
• Wandering Collectors - may now despawn from distance.
• Warthog - health values are decreased.
TECHNICAL:
• Mob Spawn Eggs are retextured.
• Changed all of the blocks geometry filename.
• Changed all of the entities geometry and texture filename.
• Rearranged all of the Item component.




[Version: 1.3]
     "New Additional"
MOBS:
• Added Cave Wolf
• Added Clurve
• Added Deepslate Golem
• Added Sculk Walker
ITEMS:
• Added Enchanted Emerald
• Added Raw Clurve Flesh
• Added Cooked Clurve Flesh

     "Changes & Fixes"
GENERAL:
• Updated for compatibility with Minecraft version 1.19.40.
MOBS:
• Cave Dweller may now drop an Emerald.
• Cave Scorpion spawning is improved.
• Cursed Book can attack the player's tamed mob, but not include the vanilla's tamable mobs.
• Cursed Scarecrow will no longer generate bubbles on water.
• Enchanter Apprentice may now drop an Enchanted Emerald instead of Emerald.
• Firefly spawn rate is increased.
• Forest Guardian spawn rate is decreased.
• Prismarine Clam may now drop Experience Orbs on death.
• Wandering Collector spawn rate is decreased.
• Cobblestone Golem spawn rate is decreased.
• Mossy Cobblestone Golem spawn rate is decreased.
• Changed the taming item of Cobblestone & Mossy Cobblestone Golem to Enchanted Emerald.
• Decreased the collision box of both the Cobblestone Golem.



[Version: 1.2]
     "New Additional"
MOBS:
• Added Cave Dweller
• Added Magma Golem
ITEMS:
• Added Wooden Club

     "Changes & Fixing"
GENERAL:
• Updated and fixed to get compatible for Minecraft version 1.19.30.
MOBS:
• Cave Scorpion light level to spawn is lowered.
• Cursed Scarecrow can now float on water.
• Fixed the Forest Guardian for not spawning naturally.
• Forest Guardian attack damage is decreased from "5-8" to "4-5".
• Forest Guardian can now float on water.
• Forest Guardian health is decreased from "60" to "50".
• Cobblestone Golem light level to spawn is lowered.
• Cobblestone Golem health is decreased from "80" to "60".
• Mossy Cobblestone Golem spawn rate is decreased.
• Mossy Cobblestone Golem health is decreased from "60" to "40".
BLOCKS:
• Due to Minecraft version 1.19.30 update, all of the custom blocks are burnable or they will catch fire and burned.


[Version: 1.1]
     "New Additional"
MOBS:
• Added Forest Guardian
• Added Sparrow
• Added Squirrel
ITEMS:
• Added Squirrel Hide
• Added Raw Squirrel
• Added Cooked Squirrel
RECIPES:
• Added new Leather recipe using 4 Squirrel Hide.
TRADES:
• Added new trade - 5 Squirrel Hide for 1 Emerald in Wandering Collector trades.
• Added new trade - 5 Raw Squirrel for 1 Emerald in Wandering Collector trades.

     "Changes & Fixing"
• Enchanter Apprentice and Cursed Book will attack the Forest Guardian.
• Changed the recipe file path that causing mod to not working on Window/Xbox.
• Cursed Scarecrow will no longer attack the villager & other animals.
• Increased Giant Moa attack damage from "2" to "5".
• Decreased Wandering Collector spawn rate.
• Changed Mud Cube, Sand Cube, & Snow Cube geometry.
• Changed the Mud Soil light absorption from "1" to "15".
• Fixed the error that causes the other blocks to be missing in the Minecraft v1.19.



[Version: 1.0]
MOBS:
• Added Giant Moa
• Added Forest Wisp
• Added Skeleton Footman
• Added Skeleton Guard
• Added Skeleton Swordman
• Added Skeleton Golden Swordman
• Added Brown Mushroom
• Added Red Mushroom
• Added Prismarine Clam
• Added Jellyfish
• Added Mossy Cobblestone Golem
• Added Cobblestone Golem
• Added Wandering Collector
• Added Warthog
• Added Dire Bat
• Added Mud Cube
• Added Snow Cube
• Added Sand Cube
• Added Cursed Scarecrow
• Added Scorpion
• Added Desert Scorpion
• Added Badlands Scorpion
• Added Cave Scorpion
• Added Cursed Book
• Added Enchanter Apprentice

BLOCKS:
• Added Mud Soil
• Added Mud Soil Bricks
• Added Jellyfish Slime Block

ITEMS:
• Added Raw Moa
• Added Cooked Moa
• Added Scorpion Telson
• Added Mud Ball
• Added Jellyfish Slime
• Added Bucket of Jellyfish
• Added Bucket of Prismarine
• Added Scorpion Gauntlet



