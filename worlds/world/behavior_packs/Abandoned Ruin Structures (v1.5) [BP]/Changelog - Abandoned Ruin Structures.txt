Abandoned & Ruin Structures

[Version: 1.5]
          "New Additional"
STRUCTURES:
• Added Abandoned Castle
• Added Abandoned Plain Chapel
• Added Abandoned Taiga House (6 variants)
• Added Ancient Tomb
• Added Creeper Head Statue
• Added Grave (9 variants)

          "Changes & Fixes"
GENERAL:
• Updated for compatibility with Minecraft version 1.19.60.
STRUCTURES:
• Abandoned Lumberjack House - will now generate naturally.
• Abandoned Cave House - generating rate is decreased.
• Abandoned Cave Library - generating rate is decreased.
• Abandoned Plain House - will no longer generate on Taiga.
• Ancient Castle - generation is improved.
• Ancient Fort - will now generate too on ocean underground.
• Ancient Nether Portal - will now generate too on ocean underground.
• Ancient Vex Fort - will now generate too on ocean underground.
• Cave Dweller House - will now generate too on ocean underground.
• Giant Fallen Spruce Log - will now generate too on Grove.
• Illager Fort - will now generate too on Savanna & Badlands.
• Illager Garrison - will now generate too on Savanna.
• Large Fossil - will now generate too on Frozen Ocean.
• Miners Tent - generate rate is decreased.
TECHNICAL:
• Shortened all the filenames of the structures, so they can easily identified & type on command.
• Changed the structure name of "Ancient Sculptures" to "Ruin Ancient Sculptures".
• Changed the structure name of "Ancient Fortress" to "Ancient Fort".
• Changed the structure name of "Ancient Garrison" to "Ancient Vex Fort".
• Changed the structure name of "Pillager Barracks" to "Illager Fort".
• Changed the structure name of "Pillager Garrison" to "Illager Garrison".
• Changed the structure name of "Abandoned Watchtower" to "Wooden Watchtower".
• Changed the structure name of "Wooden Skull" to "Wooden Skull Post".



[Version: 1.4]
     "New Additional"
STRUCTURES:
• Added Ancient Castle
• Added Ancient Fortress
• Added Ancient Nether Portal
• Added Sculk Pit
• Added Cave Dweller House (2 variants)
• Added Ancient Sculpture (5 variants)
• Added Small Ancient Ruin (5 variants)
• Added 2 new Small Cave Ruin variants.

     "Changes & Fixing"
• Updated and fixed to get compatible for Minecraft version 1.19.41.
• Ruin Column will now generate unburied.
• Small Cave Ruin will now generate unburied.
• Tomb generating chance is decreased in underground and increased in surface.
• Abandoned Cave Library generating chance is decreased.

[Version: 1.3]
     "New Additional"
STRUCTURES:
• Added Ancient Garrison
• Added Illager Barracks
• Added Abandoned Cave Library
• Added Abandoned Cave House (3 variants)
• Added Tomb
• Added Miners Tent (2 variants)
• Added Ruin Column (6 variants)
• Added Small Cave Ruin (4 variants)


     "Changes & Fixing"
• Updated and fixed to get compatible for Minecraft version 1.19.30.


[Version: 1.2]
     "New Additional"
STRUCTURES:
• Added Jungle Pyramid
• Added Hobbit House (2 variants)
• Added Giant Fallen Oak Log
• Added Giant Fallen Spruce Log
• Added Giant Fallen Birch Log
• Added Giant Fallen Jungle Log
• Added Giant Fallen Acacia Log
• Added Giant Fallen Dark Oak Log
• Added Giant Fallen Mangrove Log
• Added Ruin Obelisk
• Added Abandoned Watchtower (6 variants)
• Added Oak Bench (4 variants)
• Added Spruce Bench (4 variants)
• Added Jungle Bench (4 variants)
• Added Acacia Bench (4 variants)
• Added Dark Oak Bench (4 variants)
• Added Mangrove Bench (4 variants)
• Added Haybale Pile (3 variant)



[Version: 1.1]
     "New Additional"
STRUCTURES:
• Added Abandoned Lumberjack House with 2 variants.
• Added 2 new Abandoned Badlands Chapel variant & loots.
• Added 2 new Abandoned Desert Chapel variant & loots.
• Added 6 new Abandoned Desert House variant & loots.
• Added 6 new Abandoned Jungle House variant & loots.
• Added 6 new Abandoned Plain House variant & loots.
• Added 6 new Abandoned Savanna House variant & loots.
• Added 1 new Abandoned Tower variant & loots.
• Added 2 new Copper Vault variant & loots.
• Added 1 new Skeleton Crypt variant & loots.
• Added 2 new Zombie Crypt variant & loots.
• Added 2 new Ice Tower variant & loots.
• Added 8 new Pillager Camp variant & loots.
• Added 1 new Pillager Tower variant & loots.

     "Changes & Fixing"
• Pillager Camp - now had 80% chance to spawn 1 Villager inside the cage & 2 Pillager on camp.
• Abandoned Plain House - now had 75% chance to spawn 1 Zombie Villager inside.
• Abandoned Desert House - now had 25% chance to spawn 1 Zombie Villager inside.
• Abandoned Jungle House - now had 50% chance to spawn 1 Zombie Villager inside.
• Abandoned Savanna House - now had 50% chance to spawn 1 Zombie Villager inside.
• Abandoned Tower - now had 75% chance to spawn 2 Pillager inside.
• Copper Vault - now had 75% chance to spawn 1 Zombie Villger inside.
• Ice Tower - now had 50% chance to spawn 1 Pillger inside.
• Witch Hut Tower - now had 80% to spawn 1 Witch inside.
• Abandoned Plain House - generating chance is decreased.
• Abandoned Tower - generating chance is decreased.
• Small Forest Ruins - generating chance is decreased.
• Zombie Crypt - generating chance is decreased.
• Skeleton Crypt - generating chance is decreased.
• Fossils - generating chance is decreased.
• Ice Tower - generating chance is decreased.
• Pillager Camp - generating chance is decreased.
• Pillager Tower - generating chance is decreased.
• Pillager Garrison - generating chance is decreased.
• Skull - generating chance is decreased.
• Scarecrow - generating chance is decreased.
• Witch Hut Tower - generating chance is decreased.
• Witch Hut Tower - removed the Witch spawner inside.
• Witch Hut Tower - barrel now had loot inside.
• Updated to get compatible for Minecraft version 1.18.12 and 1.19.0.20 (BETA).
• Recoded some data & folder path of this Add-On.
• Rearranged the description.
KNOWN BUGS/PROBLEMS:
• Some structures had an implacable area or part, due to the invisible block that spawn the mob inside the structure. Those are not permanent, they will disappear after the mob are spawned.



[Version: 1.0]
     "Additional"
• Abandoned Plain House (3 variants)
• Abandoned Desert House (3 variants)
• Abandoned Savanna House (3 variants)
• Abandoned Jungle House (3 variants)
• Abandoned Desert Chapel
• Abandoned Badlands Chapel
• Abandoned Tower
• Abandoned Tower Dungeon
• Ruin Forest House (3 variants)
• Ruin Desert House (3 variants)
• Ruin Jungle Shrine (3 variants)
• Small Jungle Ruins (14 variants)
• Small Forest Ruins (4 variants)
• Small Desert Ruins(6 variants)
• Small Badlands Ruins (4 variants)
• Fossils (3 variants)
• Lost Ship (2 variants)
• Zombie Crypt
• Skeleton Crypt
• Giant Mound
• Copper Vault
• Witch Hut Tower
• Ice Tower
• Grand Ice Tower
• Pillager Camp
• Pillager Tower
• Pillager Garrison
• Skull (10 variants)
• Scarecrow (5 variants)
• Post (5 variants)
• Lantern Post (5 variants)
• Banner Post (5 variants)
• Campfire (5 variants)
• Tent (5 variants)
• Well (5 variants)
