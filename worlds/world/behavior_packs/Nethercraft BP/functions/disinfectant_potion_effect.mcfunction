effect @a[scores={merc_po=1..}] wither 0 0
effect @a[scores={merc_po=1..}] poison 0 0 true
effect @a[scores={merc_po=1..}] fatal_poison 0 0 true
execute @a[scores={merc_po=1..}] ~ ~ ~ particle minecraft:mobspell_emitter
scoreboard objectives add merc_po dummy
scoreboard players add @a merc_po 0
scoreboard objectives add merc_dru dummy
scoreboard players add @a merc_dru 0
scoreboard players remove @a[scores={merc_po=1..}] merc_po 1
effect @a[scores={merc_dru=5}] wither 130 1
scoreboard players set @a[scores={merc_dru=5}] merc_po 0
scoreboard players set @a[scores={merc_dru=5}] merc_dru 0