tag @a add baubleeffect

scoreboard objectives add timer dummy timer
scoreboard players add @a timer 1
scoreboard objectives add pottimer dummy pottimer
scoreboard players add @a pottimer 1
scoreboard objectives add randompot dummy randompot
scoreboard objectives add baublesring dummy baublesring
scoreboard players add @a baublesring 0
scoreboard objectives add baublesamulet dummy baublesamulet
scoreboard players add @a baublesamulet 0
scoreboard objectives add baublescharm dummy baublescharm
scoreboard players add @a baublescharm 0
execute @a[scores={timer=50..}] ~ ~ ~ scoreboard players set @s timer 0
execute @a[scores={pottimer=1000..}] ~ ~ ~ scoreboard players set @s pottimer 0

execute @a[scores={baublesring=1}] ~ ~ ~ effect @s fire_resistance 1 0 true
execute @a[scores={baublesring=2}] ~ ~ ~ effect @s invisibility 1 0 true
execute @a[scores={baublesring=3}] ~ ~ ~ scoreboard players add @s jump_boosteffect 2
execute @a[scores={baublesring=4}] ~ ~ ~ effect @s night_vision 1 0 true
execute @a[scores={baublesring=5}] ~ ~ ~ scoreboard players add @s regeneffect 2
execute @a[scores={baublesring=6}] ~ ~ ~ effect @s slow_falling 1 0 true
execute @a[scores={baublesring=7}] ~ ~ ~ scoreboard players add @s speedeffect 2
execute @a[scores={baublesring=8}] ~ ~ ~ scoreboard players add @s strengtheffect 2
execute @a[scores={baublesring=9}] ~ ~ ~ scoreboard players add @s resistanceeffect 2
execute @a[scores={baublesring=9}] ~ ~ ~ scoreboard players remove @s speedeffect 3
execute @a[scores={baublesring=10}] ~ ~ ~ effect @s water_breathing 1 0 true

execute @a[scores={baublesamulet=1}] ~ ~ ~ scoreboard players add @s strengtheffect 1
execute @a[scores={baublesamulet=1}] ~ ~ ~ scoreboard players remove @s speedeffect 1
execute @a[scores={baublesamulet=2}] ~ ~ ~ scoreboard players remove @e[r=7,type=!player] speedeffect 2
execute @a[scores={baublesamulet=2}] ~ ~ ~ execute @e[r=7,type=!player] ~ ~ ~ particle minecraft:soul_particle ~ ~ ~
execute @a[scores={baublesamulet=3,timer=0}] ~ ~ ~ effect @s hunger 5 0 true
execute @a[scores={baublesamulet=3}] ~ ~ ~ scoreboard players add @s resistanceeffect 1
execute @a[scores={baublesamulet=4}] ~ ~ ~ kill @e[type=zombie,r=5]
execute @a[scores={baublesamulet=4}] ~ ~ ~ kill @e[type=drowned,r=5]
execute @a[scores={baublesamulet=4}] ~ ~ ~ kill @e[type=husk,r=5]
execute @a[scores={baublesamulet=5,timer=0}] ~ ~ ~ tp @e[type=xp_orb,r=5] ~ ~ ~
execute @a[scores={baublesamulet=5,timer=0}] ~ ~ ~ tp @e[type=item,r=5] ~ ~ ~
execute @a[scores={baublesamulet=6}] ~ ~ ~ detect ~ ~-1 ~ grass 0 scoreboard players add @s speedeffect 2
execute @a[scores={baublesamulet=6}] ~ ~ ~ detect ~ ~-1 ~ grass 0 particle minecraft:crop_growth_emitter ~ ~ ~
execute @a[scores={baublesamulet=7,timer=0}] ~ ~ ~ scoreboard players add @e[r=7,type=!player] withereffect 2
execute @a[scores={baublesamulet=7}] ~ ~ ~ execute @e[r=7,type=!player] ~ ~ ~ particle minecraft:end_chest ~ ~ ~

execute @a[scores={baublescharm=1}] ~ ~ ~ execute @a[rx=-55,rxm=-90] ~ ~ ~ effect @s levitation 1 5 true
execute @a[scores={baublescharm=1}] ~ ~ ~ execute @a[rx=-20,rxm=-35] ~ ~ ~ effect @s levitation 1 2 true
execute @a[scores={baublescharm=1}] ~ ~ ~ execute @a[rx=-35,rxm=-55] ~ ~ ~ effect @s levitation 1 3 true
execute @a[scores={baublescharm=1}] ~ ~ ~ execute @a[rx=90,rxm=-20] ~ ~ ~ effect @s slow_falling 1 0 true
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=creeper,r=5]
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=witch,r=5]
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=vindicator,r=5]
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=pillager,r=5]
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=guardian,r=5]
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=spider,r=5]
execute @a[scores={baublescharm=2}] ~ ~ ~ kill @e[type=cave_spider,r=5]
execute @a[scores={baublescharm=3}] ~ ~ ~ scoreboard players random @s randompot 1 10
execute @a[scores={baublescharm=3,pottimer=0,randompot=1}] ~ ~ ~ summon zombie
execute @a[scores={baublescharm=3,pottimer=0,randompot=2}] ~ ~ ~ summon skeleton
execute @a[scores={baublescharm=3,pottimer=0,randompot=3}] ~ ~ ~ summon wither_skeleton
execute @a[scores={baublescharm=3,pottimer=0,randompot=4}] ~ ~ ~ summon creeper
execute @a[scores={baublescharm=3,pottimer=0,randompot=5}] ~ ~ ~ summon spider
execute @a[scores={baublescharm=3,pottimer=0,randompot=6}] ~ ~ ~ summon cave_spider
execute @a[scores={baublescharm=3,pottimer=0,randompot=7}] ~ ~ ~ summon pillager
execute @a[scores={baublescharm=3,pottimer=0,randompot=8}] ~ ~ ~ summon vindicator
execute @a[scores={baublescharm=3,pottimer=0,randompot=9}] ~ ~ ~ summon zombie
execute @a[scores={baublescharm=3,pottimer=0,randompot=10}] ~ ~ ~ summon blaze
execute @a[scores={baublescharm=4}] ~ ~ ~ scoreboard players random @s randompot 1 10
execute @a[scores={baublescharm=4,pottimer=0,randompot=1}] ~ ~ ~ effect @s regeneration 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=2}] ~ ~ ~ effect @s poison 15 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=3}] ~ ~ ~ effect @s speed 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=4}] ~ ~ ~ effect @s speed 20 1 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=5}] ~ ~ ~ effect @s fire_resistance 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=6}] ~ ~ ~ effect @s resistance 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=7}] ~ ~ ~ effect @s saturation 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=8}] ~ ~ ~ effect @s jump_boost 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=9}] ~ ~ ~ effect @s strength 20 0 true
execute @a[scores={baublescharm=4,pottimer=0,randompot=10}] ~ ~ ~ effect @s absorption 20 0 true
execute @a[scores={baublescharm=5}] ~ ~ ~ scoreboard players add @s speedeffect 2
execute @a[scores={baublescharm=5}] ~ ~ ~ scoreboard players add @s resistanceeffect 1
execute @a[scores={baublescharm=5}] ~ ~ ~ scoreboard players add @s strengtheffect 1
execute @a[scores={baublescharm=5}] ~ ~ ~ scoreboard players add @s jump_boosteffect 2