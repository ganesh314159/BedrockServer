execute @s[scores={baublesamulet=1}] ~ ~ ~ give @s vatonage:blood_amulet
execute @s[scores={baublesamulet=2}] ~ ~ ~ give @s vatonage:chilling_amulet
execute @s[scores={baublesamulet=3}] ~ ~ ~ give @s vatonage:leeching_amulet
execute @s[scores={baublesamulet=4}] ~ ~ ~ give @s vatonage:love_amulet
execute @s[scores={baublesamulet=5}] ~ ~ ~ give @s vatonage:magic_amulet
execute @s[scores={baublesamulet=6}] ~ ~ ~ give @s vatonage:nature_amulet
execute @s[scores={baublesamulet=7}] ~ ~ ~ give @s vatonage:wither_amulet

clear @s vatonage:love_amulet 0 1
scoreboard players set @s baublesamulet 4