execute @s[scores={baublescharm=1}] ~ ~ ~ give @s vatonage:angel_charm
execute @s[scores={baublescharm=2}] ~ ~ ~ give @s vatonage:death_charm
execute @s[scores={baublescharm=3}] ~ ~ ~ give @s vatonage:mob_charm
execute @s[scores={baublescharm=4}] ~ ~ ~ give @s vatonage:potion_charm
execute @s[scores={baublescharm=5}] ~ ~ ~ give @s vatonage:power_charm

clear @s vatonage:mob_charm 0 1
scoreboard players set @s baublescharm 3