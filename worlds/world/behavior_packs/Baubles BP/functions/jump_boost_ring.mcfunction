execute @s[scores={baublesring=1}] ~ ~ ~ give @s vatonage:fire_resistance_ring
execute @s[scores={baublesring=2}] ~ ~ ~ give @s vatonage:invisibility_ring
execute @s[scores={baublesring=3}] ~ ~ ~ give @s vatonage:jump_boost_ring
execute @s[scores={baublesring=4}] ~ ~ ~ give @s vatonage:night_vision_ring
execute @s[scores={baublesring=5}] ~ ~ ~ give @s vatonage:regeneration_ring
execute @s[scores={baublesring=6}] ~ ~ ~ give @s vatonage:slow_falling_ring
execute @s[scores={baublesring=7}] ~ ~ ~ give @s vatonage:speed_ring
execute @s[scores={baublesring=8}] ~ ~ ~ give @s vatonage:strength_ring
execute @s[scores={baublesring=9}] ~ ~ ~ give @s vatonage:turtle_master_ring
execute @s[scores={baublesring=10}] ~ ~ ~ give @s vatonage:water_breathing_ring

clear @s vatonage:jump_boost_ring 0 1
scoreboard players set @s baublesring 3