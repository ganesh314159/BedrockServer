execute @s[scores={baublesamulet=1}] ~ ~ ~ give @s vatonage:blood_amulet
execute @s[scores={baublesamulet=2}] ~ ~ ~ give @s vatonage:chilling_amulet
execute @s[scores={baublesamulet=3}] ~ ~ ~ give @s vatonage:leeching_amulet
execute @s[scores={baublesamulet=4}] ~ ~ ~ give @s vatonage:love_amulet
execute @s[scores={baublesamulet=5}] ~ ~ ~ give @s vatonage:magic_amulet
execute @s[scores={baublesamulet=6}] ~ ~ ~ give @s vatonage:nature_amulet
execute @s[scores={baublesamulet=7}] ~ ~ ~ give @s vatonage:wither_amulet

scoreboard players set @s baublesamulet 0

execute @s[scores={baublesring=1}] ~ ~ ~ give @s vatonage:fire_resistance_ring
execute @s[scores={baublesring=2}] ~ ~ ~ give @s vatonage:invisibility_ring
execute @s[scores={baublesring=3}] ~ ~ ~ give @s vatonage:jump_boost_ring
execute @s[scores={baublesring=4}] ~ ~ ~ give @s vatonage:night_vision_ring
execute @s[scores={baublesring=5}] ~ ~ ~ give @s vatonage:regeneration_ring
execute @s[scores={baublesring=6}] ~ ~ ~ give @s vatonage:slow_falling_ring
execute @s[scores={baublesring=7}] ~ ~ ~ give @s vatonage:speed_ring
execute @s[scores={baublesring=8}] ~ ~ ~ give @s vatonage:strength_ring
execute @s[scores={baublesring=9}] ~ ~ ~ give @s vatonage:turtle_master_ring
execute @s[scores={baublesring=10}] ~ ~ ~ give @s vatonage:water_breathing_ring

scoreboard players set @s baublesring 0

execute @s[scores={baublescharm=1}] ~ ~ ~ give @s vatonage:angel_charm
execute @s[scores={baublescharm=2}] ~ ~ ~ give @s vatonage:death_charm
execute @s[scores={baublescharm=3}] ~ ~ ~ give @s vatonage:mob_charm
execute @s[scores={baublescharm=4}] ~ ~ ~ give @s vatonage:potion_charm
execute @s[scores={baublescharm=5}] ~ ~ ~ give @s vatonage:power_charm

scoreboard players set @s baublescharm 0