scoreboard objectives add candp dummy
scoreboard players set @s candp 0
execute @s ~ ~ ~ detect ~ ~ ~ chest -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ bedrock -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ barrel -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ shulker_box -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ undyed_shulker_box -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ portal -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ end_portal_frame -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ frame -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ end_portal -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ dragon_egg -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ command_block -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ repeating_command_block -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ deny -1 scoreboard players set @s candpe 1
execute @s ~ ~ ~ detect ~ ~ ~ allow -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ barrier -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ structure_block -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ structure_void -1 scoreboard players set @s candp 1
execute @s ~ ~ ~ detect ~ ~ ~ netherite_block -1 scoreboard players set @s candp 1
execute @s[scores={candp=0}] ~ ~ ~ structure save trzu ~~~ ~~~ false memory true