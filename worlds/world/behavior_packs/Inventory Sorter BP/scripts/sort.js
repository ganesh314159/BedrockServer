import {ItemTypes,world as World,ItemStack} from '@minecraft/server';
    
const effectCallback = World.events.effectAdd;
effectCallback.subscribe(effectFunction);
	  
async function effectFunction(effectData){	
	let effectName = effectData.effect.displayName;
	let effectAmp = effectData.effect.amplifier;
	let entity = effectData.entity;	
	let dimension = entity.dimension;
	let itemArray = [];
	if((effectAmp == 10) && entity.typeId == 'minecraft:player'){
		let container = entity.getComponent('inventory').container;
		//get all items except for hotbar items
		for(let slot=0; slot<container.size; slot++){
			if((slot<=8 && !container.getItem(slot))){
				container.setItem(slot,new ItemStack(ItemTypes.get('Minecraft:barrier'),1));
				continue;
			}else if( slot<=8 || !container.getItem(slot)){
				continue;	
			}else{
				itemArray.push(container.getItem(slot));
				container.setItem(slot,null);
			}
		}
		//sort items
		let promise = new Promise((resolve)=>{
		try{
		itemArray.sort((a, b) => {
		let first;
		let second;
		
		if(a.nameTag){
			first = a.nameTag;
		}else{
			first = a.typeId.split(':')[1];
		}
		
		if(b.nameTag){
			second = b.nameTag;
		}else{
			second = b.typeId.split(':')[1];
		}
		
		if(first > second){
			return 1;
		}else if(first == second){
			return 1;
		}else{
			return -1;
		}	
		});	
		for(let item of itemArray){
			container.addItem(item);
		}
		entity.onScreenDisplay.setActionBar(`§6Inventory Sorted!`);
		entity.runCommandAsync(`clear @s barrier`);
		resolve();	
		}catch(e){
		console.warn(`${e}`);	
		}
		});
		await promise;
		
	}	
}